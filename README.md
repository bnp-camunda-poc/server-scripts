# Scripts

Import all necessary projects from https://gitlab.com/bnp-camunda-poc into this folder as ROOT. Depending on the setup all scripts executed from this folder should work seamlessly. Adjust the shell scripts when changing the names of the folders.

## Running projects
There is an execution order for Camunda and Backend. The front-end is seperate from both, however resources available can slow or freeze the process.
### Run order
```sh
sh ./serve_cust_fe.sh
sh ./serve_camunda.sh
sh ./serve_be.sh
```

## Building projects
There is a Building order you should adhere. This decreases chances on issues with resources.
### Build order
```sh
sh ./build_cust_fe.sh
sh ./build_camunda.sh
sh ./build_be.sh
```

## Monitor progress (Building & Running process)
All applications are running in nohup. to check for errors run `cat ./nohup.out` for a simple print or `nano ./nohup.out` for reading the document. (in case when nano isn't installed use `vi` or `vim`)

## Cleaning (Recommended | Not Required)

Remove all stop scripts after stopping the services. It is not necessary to do this, but will accumulate false positive errors when trying to kill non-exisiting task-ids. Best is to do this after a system reset.
